import React from "react";
import Card from "./components/Card";
import Product from "./components/Product";

function App() {
  // const [product, setProduct{}] = useState([]);

  // const [ProductUpdate, setProductUpdate] = useState(false);

  // const fetchProduct = async () => {
  //   try {
  //     const response = await axios.get("https://api-project.amandemy.co.id/api/products");
  //     setProduct(response.data.data);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  // useEffect(() => {
  //   fetchProduct();
  // }, []);

  return (
    <div className="Form">
      <div className="my-10">
        <div className="shadow-xl mx-10 rounded-xl">
          <Card />
        </div>

        <div className="shadow-xl mx-10 rounded-xl my-10 py-2">
          <h1 className="text-blue-500 font-medium mx-10 my-10 text-3xl">Daftar Produk</h1>
          <div className="flex flex-wrap">
            <Product className="my-10 py-10" text="Nama Produk" price="Rp. 240.000,-" />
            <Product className="my-10 py-10" text="Nama Produk 2" price="Rp. 300.000,-" />
            <Product className="my-10 py-10" text="Nama Produk 3" price="Rp. 210.000,-" />
            <Product className="my-10 py-10" text="Nama Produk 4" price="Rp. 195.000,-" />
            <Product className="my-10 py-10" text="Nama Produk 5" price="Rp. 180.000,-" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
