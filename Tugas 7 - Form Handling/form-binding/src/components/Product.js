import React from "react";
import { useEffect, useState } from "react";

function App() {
  const [produk, setProduk] = useState([]);

  const fetchProduct = async () => {};

  useEffect(() => {
    fetchProduct();
  }, []);
}

const fetchProduct = async () => {
  try {
    const response = await axios.get("https://api-project.amandemy.co.id/api/articles");
    setProduk(response.data.data);
  } catch (error) {
    console.log(error);
  }
};

function Product(props) {
  return (
    <div>
      <div class="flex max-w-sm bg-white py-5 px-2 mx-4 my-4 border-2 border-gray-100 rounded-lg shadow-md hover:shadow-xl hover:bg-blue-50">
        <a href="#">
          <img class="box-content h-48 w-96 items-top object-cover md:w-48 rounded-md mx-2 my-2" src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSqP8cyeuu4xRjAgKlV1q98T9LUqXrYWNsDkqBi_Hg5S9TlZevd" />
          <p class="py-1 text-1xl font-semibold text-blue-900 px-2">{props.text}</p>
          <p class="font-regular text-blue-800 px-2">{props.price}</p>
        </a>
      </div>
    </div>
  );
}

async function item() {
  const response = await fetch("https://api-project.amandemy.co.id/api/products");
  const item = await response.json();
  console.log(item);
}

export default Product;
