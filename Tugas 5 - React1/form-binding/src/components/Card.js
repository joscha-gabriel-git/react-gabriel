import React from "react";

function Card() {
  return (
    <div className="Card">
      <div className="mx-10 my-10">
        <div className="my-2 text-blue-500">
          <h1 className="flex font-semibold text-3xl my-10">Create Product</h1>
        </div>
        <div className="container">
          <form action="/">
            <div className="flex">
              <div className="container">
                <label className="text-blue-500 font-medium" for="fname">
                  Nama Barang:
                </label>
                <input placeholder="Nama Produk" className="w-96 m-1 border-2 border-blue-100 shadow-sm rounded-lg" id="fname" name="fname" value="" />
              </div>

              <div className="container">
                <label className="text-blue-500 font-medium" for="stok">
                  Stock Barang:
                </label>
                <input placeholder="5" type="number" className="w-96 m-1 border-2 border-blue-100 shadow-sm rounded-lg" id="stok" name="stok" value="" />
              </div>
            </div>

            <br />
            <div className="flex">
              <div className="container">
                <label className="text-blue-500 font-medium" for="stok">
                  Harga Barang:
                </label>
                <input placeholder="contoh 8000" type="number" className="w-96 m-1 border-2 border-blue-100 shadow-sm rounded-lg" id="stok" name="stok" value="" />
              </div>

              <div className="container">
                <label className="text-blue-500 font-medium" for="stok">
                  Harga Diskon:
                </label>
                <input placeholder="contoh 5000" type="number" className="w-96 m-1 border-2 border-blue-100 shadow-sm rounded-lg" id="stok" name="stok" value="" />
              </div>
            </div>

            <br />
            <div className="flex">
              <div className="container">
                <label className="text-blue-500 font-medium" for="stok">
                  Tipe Barang:
                </label>
                <select className="w-96 m-1 border-2 border-blue-100 shadow-sm rounded-lg" id="stok" name="stok" value="Teknologi">
                  <option value="Teknologi">Teknologi</option>
                  <option value="Seni">Seni</option>
                  <option value="Desain">Desain</option>
                  <option value="Makanan Kering">Makanan Kering</option>
                </select>
              </div>

              <div className="container">
                <label className="text-blue-500 font-medium" for="stok">
                  Link Foto:
                </label>
                <input type="text" className="w-96 m-1 border-2 border-blue-100 shadow-sm rounded-lg" id="stok" name="stok" value=""></input>
              </div>
            </div>

            <br />
            <div>
              <label className="align-top text-blue-500 font-medium" for="stok">
                Deskripsi:
              </label>
              <textarea className="w-96 m-1 border-2 border-blue-100 shadow-sm rounded-lg" id="stok" name="stok" value=""></textarea>
            </div>
          </form>

          <button className="bg-white text-blue-600 rounded-lg border-blue-500 font-semibold text-white my-10 border-2 px-5 py-2 shadow-md hover:shadow-xl hover:bg-blue-400 hover:text-blue-900" value="submit">
            Cancel
          </button>

          <button className="bg-blue-500 rounded-lg font-semibold text-white my-10 border-2 px-5 py-2 shadow-md hover:shadow-xl hover:bg-blue-400 hover:text-blue-900 mx-2" value="submit">
            Create
          </button>
        </div>
      </div>
    </div>
  );
}

export default Card;
