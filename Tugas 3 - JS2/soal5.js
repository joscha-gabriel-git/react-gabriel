const arrObj = [
  {
    name: "Headset",

    price: 250000,
  },

  {
    name: "Handphone",

    price: 150000,
  },

  {
    name: "Laptop",

    price: 850000,
  },
];

const newarray = arrObj.map(({ name, price }) => ({ price }));

console.log(newarray);
