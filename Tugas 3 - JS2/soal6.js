const arr = [5, 3, 4, 7, 6, 9, 2];

const newArr = arr.map(myFunction);

function myFunction(angka) {
  if (angka % 2 == 0) {
    return angka * 2;
  } else {
    return angka * 3;
  }
}

console.log(newArr);
